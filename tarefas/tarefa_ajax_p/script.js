let std_url = "https://treinamentoajax.herokuapp.com/messages/";
let name = document.querySelector('#name-input-box').value;
let input_content = document.querySelector('#message').value;
let input_button = document.querySelector('#message-button');
let receive_button = document.querySelector('#r-button');
let test_button = document.querySelector('#t-button');
let mail_box = document.querySelector('.incoming-messages');
var new_messages;

function send(){
    const fetchBody = {
        "message" : {
            "name": name,
            "message": input_content
        }

    }

    const fetchConfig = {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body:   JSON.stringify(fetchBody)
    }

    fetch(std_url, fetchConfig)
}


function receive(){
    fetch(std_url)
    .then(response => response.json)
    .then(response => {
        for (i in response){
            let card = document.createElement("article");
            let h1 = document.createElement("h1");
            let p =  document.createElement("p");
            h1.innerHTML = response[i].name;
            p.innerHTML = response[i].message;
            card.appendChild(h1);
            card.appendChild(p);
            card.id = response[i].id;
            mail_box.appendChild(card);

}})}

input_button.addEventListener('click', send);
receive_button.addEventListener('click', receive);