//globais
var nomes = ["A", "B", "C", "D"];

//funções
function DividirTurmas(pessoa, turmas) {
    //retorna 0 - A / 1 - B / 2 - C / 3 - D
    for(j=0; j < nomes.length; j++){
        if(pessoa["turma"] == nomes[j]){
            return j
        }
    }
    console.log("ERRO: TURMA NÃO ENCONTRADA.")
}

function CalcularMedia(pessoa) {
    //Retorna a média de uma pessoa
    let media = (pessoa["nota1"] + pessoa["nota2"]) / 2;
    return media
}

//locais
let todos = [
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Maria",
        "turma": "A",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Johnathan",
        "turma": "A",
        "nota1": 9,
        "nota2": 9
    },
    {
        "nome": "Bernardo",
        "turma": "B",
        "nota1": 7,
        "nota2": 3
    },
    {
        "nome": "Julia",
        "turma": "B",
        "nota1": 4,
        "nota2": 6
    },
    {
        "nome": "Luana",
        "turma": "C",
        "nota1": 4,
        "nota2": 5
    },
    {
        "nome": "Leonardo",
        "turma": "C",
        "nota1": 9,
        "nota2": 3
    },
    {
        "nome": "Andre",
        "turma": "C",
        "nota1": 4,
        "nota2": 8
    },
    {
        "nome": "Joao Pro ++ Premium",
        "turma": "C",
        "nota1": 10,
        "nota2": 10
    }
];
let maioresMedias = [null, null, null];

//main - Seleciona a pessoa com a maior media de cada turma em uma só lista.
for (i=0; i < todos.length; i++) {
    
    let aluno = todos[i];
    let concorrente = maioresMedias[DividirTurmas(aluno)];
    aluno["media"] = CalcularMedia(aluno);

    
    if(concorrente == null || aluno["media"] > concorrente["media"]){
        //salve Dante.
        maioresMedias[DividirTurmas(aluno)] = aluno;
        
    }
}

//Mostrar

for(i=0; i < maioresMedias.length; i++){
    let mensagem = 'O aluno ' + maioresMedias[i]["nome"] + ' da turma ' + maioresMedias[i]["turma"] + ' conseguiu a maior média, com ' + maioresMedias[i]["media"] + ' pontos.';
    console.log(mensagem);
}
//

