function checar(elem, base){
    for (letra in elem){
        if (!base.includes(elem[letra])){
            return false 
        }
    }
    return true
}

function main(array){
    let final_array = [];

    for(item in array[1]){
        if(checar(array[1][item], array[0])){
            final_array[final_array.length] = array[1][item];
        }
    }
    
    return final_array
}

let array1 = ["ab", ["abc", "ba", "ab", "bb", "kb"]]; //[base, elem]
let array2 = ["pkt", ["pkt", "pp", "pata", "po", "kkkkkkk"]];

console.log(main(array1));
console.log("===========");
console.log(main(array2));

