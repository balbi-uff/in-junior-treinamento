class Usuarios
    @@emails = {"aluno1@email.com" => "A0001", "professor1@email.com" => "P0001"}   #test - isso deve começar vazio
    @@senhas = {"A0001" => "1234","P0001" => "1234"} #matr => senha //              #test - isso deve começar vazio

    def initialize(email, senha)
        if @@senhas[@@emails[email]] == senha
            @login = true
            puts "Acesso Permitido!"
        else
            @login = false
            puts "Acesso Negado!"
        end
    end
    def atualiza_dados(nome, nasc)
        if @login
            @nome = nome
            @nasc = nasc
        else
            puts "PUTS GRILA, você não está logado."
        end
    end
    def idade
        if @login
            puts "Sua idade é #{2020 - @nasc}"
            return 2020 - @nasc
        end
    end

    def deslogar
        if @login
            @login = false
            puts "Login Retirado!"
        end
    end

end
class Turma 
    def initialize(nome, horario, dias_sem, inscritos, inscricaoA)
        puts "TURMA #{nome} CRIADA! "
        @nome_turma = nome
        @horario = horario      
        @dias_sem = dias_sem
        @inscritos = inscritos
        @inscricaoA = inscricaoA
    end
    def adicionar_inscrito(novo)
        @inscritos.append(novo)
    end
    def status_nome
        return @nome_turma
    end
    def abrir_inscricao
        @inscricaoA = true
        puts "Inscrições da Turma #{@nome_turma} ABERTAS!"
    end
    
    def fechar_inscricao
        @inscricaoA = false
        puts "Inscrições da Turma #{@nome_turma} FECHADAS!"
    end
    
    def mostrar_dados #DEBUG DEF
        puts @inscritos
    end
    def status_inscricoes
        return @inscricaoA
    end
    def adicionar_aluno(nome_aluno)
        @inscritos.append(nome_aluno)
        puts "Aluno #{nome_aluno} - Inscrito na Turma #{@nome_turma}"
    end
end

class Aluno 
    def initialize(mat, pl, curso, turmas)
        @mat = mat
        @pl = pl
        @curso = curso
        @turmas = turmas
    end
    
    def inscrever(nome_turma)
        if nome_turma.status_inscricoes
            @turmas.append(nome_turma.status_nome) 
            nome_turma.adicionar_aluno(@mat)
        else
            puts "Erro - Inscrições encerradas. "
        end
    end

    def status_nome_aluno
        return @mat
    end
end

class Professor 
    def initialize(matric, salario, materias)
        @matric = matric
        @salario = salario
        @materias = materias
    end
    def adicionar_materia(nova_mat)
        nova_mat.adicionar_professor(@matric)
        puts "Matéria #{nova_mat.status_nome} adicionada ao currículo do Professor #{@matric}"
        @materias.append(nova_mat.status_nome)
    end
end

class Materia
    def initialize(emt, nome, profs)
        @ementa = emt
        @nome = nome
        @profs = profs
        puts "Materia #{@nome} criada!"
    end
    def adicionar_professor(nome_prof)
        puts "Professor #{nome_prof} adicionado à matéria #{@nome}"
        @profs.append(nome_prof)
    end
    def status_nome
        return @nome
    end
end


#desenvolvido por André Balbi - balbi-uff
#tudo é bem straightforward, se não entender algo, me pergunte. abs.