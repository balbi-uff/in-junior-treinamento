=begin
#tar1
def multiplicar_itens(arr)
    soma = 0
    mult = 1
    for i in arr 
        for j in i
            soma += j
            mult *= j
        end
    end
    puts "Soma final: #{soma}."
    puts "Mult final: #{mult}."
end


array = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
multiplicar_itens(array)

#tar2
def dvi(x, y)
    x = x.to_f
    y = y.to_f
    puts x/y
end

dvi(5,2)
dvi(10,2)
dvi(14,7)
dvi(200,20)
dvi(98,75)

#tar3
def hsToArr(h)
    values = h.values
    new_values = []
    for i in values 
        new_values.append(i*i)
    end
    print new_values
end

hash = {"a" => 5, "b" => 30, "c" => 20}
hsToArr(hash)

#tar4
def stringer(arr)
    n_array = []
    for i in arr
        n_array.append(i.to_s)
    end
    print n_array
end


stringer([25, 35, 45])

#tar5
def divz_tr(arr)
    n_arr = []
    for i in arr
        if i%3 == 0 then
            n_arr.append(i)
        end
    end
    print n_arr
end

divz_tr([3, 6, 7, 8])